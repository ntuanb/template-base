module.exports = function(grunt) {

	grunt.initConfig({

		concat: {
			js: {
				src: [
				'./bower_components/jquery/dist/jquery.js',
				'./bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js',
				'./assets/js/lib/*.js',
				],
				dest: './assets/js/app.js'
			}
		},
		uglify: {
			options: {
				mangle: false
			},
			js: {
				files: {
					'./assets/js/app.js': './assets/js/app.js'
				}
			}
		},
		sass: {
			development: {
				files: {
					"./assets/css/app.css":"./assets/css/scss/app.scss"
				}
			}
		},
		watch: {
			js: {
				files: [
				'./bower_components/jquery/dist/jquery.js',
				'./bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap.js',
				'./assets/js/lib/*.js',
				],
				tasks: ['concat:js', 'uglify:js']
			},
			sass: {
				files: ['./assets/css/scss/*.scss'],
				tasks: ['sass']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['watch']);
}
